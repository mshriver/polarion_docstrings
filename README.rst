polarion-docstrings
===================

.. image:: https://gitlab.com/mkourim/polarion_docstrings/badges/master/pipeline.svg
    :target: https://gitlab.com/mkourim/polarion_docstrings/commits/master
    :alt: pipeline status

.. image:: https://gitlab.com/mkourim/polarion_docstrings/badges/master/coverage.svg
    :target: https://gitlab.com/mkourim/polarion_docstrings/commits/master
    :alt: coverage report

Description
-----------
Library for reading Polarion docstrings used in CFME QE. Includes a flake8 extension for validating the docstrings.

Install
-------
To install the package to your virtualenv, run

.. code-block::

    pip install polarion-docstrings

or install it from cloned directory

.. code-block::

    pip install -e .

Package on PyPI <https://pypi.python.org/pypi/polarion-docstrings>
